@echo off

for /f "usebackq tokens=*" %%i in (`findvsinstallbase.bat`) do (
    if exist "%%i\Common7\Tools\vsdevcmd\core\winsdk.bat" (
        set VSCMD_ARG_HOST_ARCH=x86
        set VSCMD_ARG_TGT_ARCH=x86
        call "%%i\Common7\Tools\vsdevcmd\core\winsdk.bat"
    )
)

shortpath.bat "%WindowsSdkDir%"
