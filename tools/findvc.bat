@echo off
setlocal enabledelayedexpansion

for /f "usebackq tokens=*" %%i in (`findvsinstallbase.bat`) do (
    set InstallDir=%%i
)

if exist "%InstallDir%\VC\Auxiliary\Build\Microsoft.VCToolsVersion.default.txt" (
    set /p Version=<"%InstallDir%\VC\Auxiliary\Build\Microsoft.VCToolsVersion.default.txt"
    set Version=!Version: =!
)

if not "%Version%"=="" (
    shortpath.bat "%InstallDir%\VC\Tools\MSVC\%Version%"
    exit /b 0
)

exit /b 1
