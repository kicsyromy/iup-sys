#[repr(i32)]
pub enum CallbackResult {
    Default = crate::iup_sys::IUP_DEFAULT,
    Close = crate::iup_sys::IUP_CLOSE,
}
