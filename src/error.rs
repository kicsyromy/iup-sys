use std::fmt::Formatter;

#[derive(Debug)]
pub enum Error {
    Utf8Error(&'static str),
    HandleAllocationFailed(&'static str),
    AppendFailed(&'static str),
    ShowFailed(&'static str),
    PopupFailed(&'static str),
    Custom(String),
    Unknown(&'static str),
}

impl Error {}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        match self {
            Error::Utf8Error(prefix) => {
                f.write_fmt(format_args!("{}: String is not valid UTF-8", prefix))
            }
            Error::HandleAllocationFailed(prefix) => f.write_fmt(format_args!(
                "{}: Failed to allocate Iup Ihandle for element",
                prefix
            )),
            Error::AppendFailed(prefix) => f.write_fmt(format_args!(
                "{}: Failed to append child to container or dialog",
                prefix
            )),
            Error::ShowFailed(prefix) => f.write_fmt(format_args!("{}: Failed to show", prefix)),
            Error::PopupFailed(prefix) => {
                f.write_fmt(format_args!("{}: Failed to show (popup)", prefix))
            }
            Error::Custom(message) => f.write_str(message.as_str()),
            Error::Unknown(prefix) => {
                f.write_fmt(format_args!("{}: An unknown error has occurred", prefix))
            }
        }
    }
}

impl std::error::Error for Error {}
