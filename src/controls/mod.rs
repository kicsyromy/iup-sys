mod text;

pub trait ControlElement<'a>: crate::element::Element<'a> {}

pub use text::*;
