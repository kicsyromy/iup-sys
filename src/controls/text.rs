use crate::{
    context::Context,
    element::{private, Element, Handle},
    error::Error,
    types::CallbackResult,
};

// FIXME: The lifetime for the VALUE and FONT properties are not correct
//        A situation could arise where the value of either of these is
//        retrieved and subsequently changed, and the compiler would
//        not pick up on this based on the lifetime parameters
pub struct Text<'a> {
    handle: Handle,
    action_cb: Option<Box<dyn FnMut(&mut Context, &mut Self) -> CallbackResult + 'a>>,
}

impl<'a> Text<'a> {
    pub fn new() -> Result<Self, Error> {
        use crate::{element::private::ElementPrivate, iup_sys::IupText};

        let native_handle = unsafe { IupText(core::ptr::null_mut()) };
        if native_handle == std::ptr::null_mut() {
            return Err(Error::HandleAllocationFailed(Self::class_name()));
        }

        Ok(Self {
            handle: Handle::from(native_handle),
            action_cb: None,
        })
    }

    pub fn set_multiline(&mut self, value: bool) {
        if value {
            set_attribute_str!(self, "MULTILINE", "YES");
        } else {
            set_attribute_str!(self, "MULTILINE", "NO");
        }
    }

    pub fn set_expand(&mut self, value: bool) {
        if value {
            set_attribute_str!(self, "EXPAND", "YES");
        } else {
            set_attribute_str!(self, "EXPAND", "NO");
        }
    }

    pub fn multiline(mut self, value: bool) -> Self {
        self.set_multiline(value);
        self
    }

    pub fn expand(mut self, value: bool) -> Self {
        self.set_expand(value);
        self
    }

    pub fn font(&self) -> Result<&'a str, Error> {
        use std::ffi::CStr;

        use crate::element::private::ElementPrivate;
        use crate::iup_sys::IupGetAttribute;

        Ok(unsafe {
            CStr::from_ptr(IupGetAttribute(
                self.handle().iup_handle,
                c_str_literal!("FONT"),
            ))
        }
        .to_str()
        .map_err(|_| Error::Utf8Error(Self::class_name()))?)
    }

    pub fn set_font(&mut self, mut font: String) {
        use crate::element::private::ElementPrivate;
        use crate::iup_sys::IupSetStrAttribute;

        if !font.ends_with('\0') {
            font.push('\0');
        }

        unsafe {
            IupSetStrAttribute(
                self.handle_mut().into(),
                c_str_literal!("FONT"),
                font.as_ptr() as *const i8,
            )
        }
    }

    pub fn value(&self) -> Result<&'a str, Error> {
        use crate::element::private::ElementPrivate;
        use crate::iup_sys::IupGetAttribute;

        let count = self.count();
        unsafe {
            std::str::from_utf8(core::slice::from_raw_parts(
                IupGetAttribute(self.handle().iup_handle, c_str_literal!("VALUE")) as *const u8,
                count,
            ))
            .map_err(|_| Error::Utf8Error(Self::class_name()))
        }
    }

    pub fn set_value(&mut self, mut content: String) {
        use crate::element::private::ElementPrivate;
        use crate::iup_sys::IupSetStrAttribute;

        if !content.ends_with('\0') {
            content.push('\0');
        }

        unsafe {
            IupSetStrAttribute(
                self.handle_mut().into(),
                c_str_literal!("VALUE"),
                content.as_ptr() as *const i8,
            )
        }
    }

    pub fn count(&self) -> usize {
        use crate::element::private::ElementPrivate;
        use crate::iup_sys::IupGetInt;

        (unsafe { IupGetInt(self.handle().iup_handle, c_str_literal!("COUNT")) }) as usize
    }
}

impl<'a> private::ElementPrivate<'a> for Text<'a> {
    fn handle_mut(&mut self) -> &mut Handle {
        &mut self.handle
    }

    fn handle(&self) -> &Handle {
        &self.handle
    }

    fn class_name() -> &'static str {
        "Text"
    }

    fn action_callback_pointer(
        &mut self,
    ) -> &mut Option<Box<dyn FnMut(&mut Context, &mut Self) -> CallbackResult + 'a>>
    where
        Self: Sized,
    {
        &mut self.action_cb
    }
}

impl<'a> Element<'a> for Text<'a> {
    fn set_title(&mut self, _: &str) {}
}
impl<'a> super::ControlElement<'a> for Text<'a> {}
