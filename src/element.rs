use crate::context::Context;
use crate::types::CallbackResult;

pub trait Element<'a>: private::ElementPrivate<'a> {
    fn set_name(&mut self, id: &str) {
        use crate::iup_sys::{IupGetName, IupSetHandle};

        let new_id = c_str!(id).into_raw();

        let old_id = unsafe { IupGetName(self.handle_mut().into()) };
        if !old_id.is_null() {
            unsafe {
                IupSetHandle(old_id as *const i8, std::ptr::null_mut());
                std::ffi::CString::from_raw(old_id);
            }
        }

        unsafe { IupSetHandle(new_id, self.handle_mut().into()) };
    }

    fn with_name(mut self, id: &str) -> Self
    where
        Self: Sized,
    {
        self.set_name(id);
        self
    }

    fn set_title(&mut self, title: &str);

    fn with_title(mut self, title: &str) -> Self
    where
        Self: Sized,
    {
        self.set_title(title);
        self
    }

    fn set_action_callback(
        &mut self,
        callback: impl FnMut(&mut crate::context::Context, &mut Self) -> crate::types::CallbackResult
            + 'a,
    ) where
        Self: Sized,
    {
        private::ElementPrivate::set_action_callback(self, Box::new(callback));
    }

    fn with_action(
        mut self,
        callback: impl FnMut(&mut Context, &mut Self) -> CallbackResult + 'a,
    ) -> Self
    where
        Self: Sized,
    {
        Element::set_action_callback(&mut self, callback);
        self
    }
}

pub struct Handle {
    pub iup_handle: *mut crate::iup_sys::Ihandle,
}

impl Handle {
    pub fn leak(&mut self) -> *mut crate::iup_sys::Ihandle {
        let handle = self.iup_handle;
        self.iup_handle = std::ptr::null_mut();

        handle
    }
}

impl Drop for Handle {
    fn drop(&mut self) {
        use crate::iup_sys::IupDestroy;
        unsafe { IupDestroy(self.iup_handle) };
    }
}

impl From<*mut crate::iup_sys::Ihandle> for Handle {
    fn from(ihandle: *mut crate::iup_sys::Ihandle) -> Self {
        Self {
            iup_handle: ihandle,
        }
    }
}

impl From<&mut Handle> for *mut crate::iup_sys::Ihandle {
    fn from(handle: &mut Handle) -> Self {
        handle.iup_handle
    }
}

pub struct HandleWrapper<'a> {
    handle: Handle,
    _marker: std::marker::PhantomData<&'a Self>,
}

impl<'a> HandleWrapper<'a> {
    pub fn wrap(native_handle: *mut crate::iup_sys::Ihandle) -> Self {
        Self {
            handle: Handle::from(native_handle),
            _marker: std::marker::PhantomData,
        }
    }
}

impl<'a> private::ElementPrivate<'a> for HandleWrapper<'a> {
    fn handle_mut(&mut self) -> &mut Handle {
        &mut self.handle
    }

    fn handle(&self) -> &Handle {
        &self.handle
    }

    fn class_name() -> &'static str {
        unimplemented!()
    }

    fn action_callback_pointer(
        &mut self,
    ) -> &mut Option<Box<dyn FnMut(&mut Context, &mut Self) -> CallbackResult + 'a>>
    where
        Self: Sized,
    {
        unimplemented!()
    }
}

impl<'a> Element<'a> for HandleWrapper<'a> {
    fn set_title(&mut self, _title: &str) {
        unimplemented!()
    }
}

pub(crate) mod private {
    pub trait ElementPrivate<'a> {
        fn handle_mut(&mut self) -> &mut super::Handle;
        fn handle(&self) -> &super::Handle;

        fn class_name() -> &'static str
        where
            Self: Sized;

        fn action_callback_pointer(
            &mut self,
        ) -> &mut Option<
            Box<
                dyn FnMut(&mut crate::context::Context, &mut Self) -> crate::types::CallbackResult
                    + 'a,
            >,
        >
        where
            Self: Sized;

        fn set_action_callback(
            &mut self,
            callback: Box<
                dyn FnMut(&mut crate::context::Context, &mut Self) -> crate::types::CallbackResult
                    + 'a,
            >,
        ) where
            Self: Sized,
        {
            use crate::{
                context::Context,
                iup_sys::{IupGetAttribute, IupSetCallback},
                types::CallbackResult,
            };

            extern "C" fn native_callback<'a, T>(this: *mut crate::iup_sys::Ihandle) -> i32
            where
                T: ElementPrivate<'a> + Sized,
            {
                let this = unsafe {
                    &mut *(IupGetAttribute(this, c_str_literal!("iup_rs_self")) as *mut T)
                };

                let mut callback: Option<Box<dyn FnMut(&mut Context, &mut T) -> CallbackResult>> =
                    None;

                std::mem::swap(this.action_callback_pointer(), &mut callback);

                let result = if callback.is_some() {
                    let result = (callback.as_mut().unwrap())(&mut Context::instance(), this);
                    if this.action_callback_pointer().is_none() {
                        std::mem::swap(this.action_callback_pointer(), &mut callback);
                    }
                    result
                } else {
                    CallbackResult::Default
                };

                result as i32
            }

            let mut new_callback = Some(callback);
            std::mem::swap(self.action_callback_pointer(), &mut new_callback);
            unsafe {
                IupSetCallback(
                    self.handle_mut().into(),
                    c_str_literal!("ACTION"),
                    Some(native_callback::<'a, Self>),
                );
            }

            self.update_self_reference();
        }

        fn update_self_reference(&mut self) {
            use crate::iup_sys::IupSetAttribute;
            unsafe {
                IupSetAttribute(
                    self.handle_mut().into(),
                    c_str_literal!("iup_rs_self"),
                    self as *mut _ as *const i8,
                )
            };
        }
    }
}
