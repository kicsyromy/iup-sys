#![windows_subsystem = "windows"]

use crate::context::Context;
use crate::menus::MenuItem;

#[macro_use]
mod utilities;

mod containers;
mod context;
mod controls;
mod dialogs;
mod element;
mod error;
mod iup_sys;
mod menus;
mod types;

fn open_callback(
    context: &mut Context,
    _this: &mut MenuItem,
) -> Result<(), Box<dyn std::error::Error>> {
    use controls::Text;
    use dialogs::*;
    use error::Error;
    use std::{fs::OpenOptions, io::Read};

    let open_dialog = FileDialog::new()?
        .with_type(FileDialogType::Open)
        .with_extension_filters("Text Files|*.txt|All Files|*.*|")
        .popup()?;

    if open_dialog.status() != FileDialogStatus::OperationCancelled {
        let file_name = open_dialog.value()?[0].clone();

        let mut file_content = Vec::new();
        let mut file = OpenOptions::new().read(true).open(&file_name)?;
        file.read_to_end(&mut file_content)?;

        let file_content =
            String::from_utf8(file_content).map_err(|_| Error::Utf8Error("open_callback"))?;

        let text_main = unsafe { context.element_by_name::<Text>("text_main") }
            .ok_or_else(|| Error::Custom("Failed to find 'text_main' element".into()))?;
        text_main.set_value(file_content);
    }

    Ok(())
}

fn save_as_callback(
    context: &mut Context,
    _this: &mut MenuItem,
) -> Result<(), Box<dyn std::error::Error>> {
    use controls::Text;
    use dialogs::*;
    use error::Error;
    use std::{fs::OpenOptions, io::Write};

    let save_dialog = FileDialog::new()?
        .with_type(FileDialogType::Save)
        .with_extension_filters("Text Files|*.txt|All Files|*.*|")
        .popup()?;

    if save_dialog.status() != FileDialogStatus::OperationCancelled {
        let mut file_name = save_dialog.value()?[0]
            .clone()
            .to_str()
            .ok_or_else(|| Error::Utf8Error("save_as_callback"))?
            .to_owned();
        if !file_name.ends_with(".txt") {
            file_name += ".txt";
        }
        let file_name = file_name.as_str();

        let mut file = OpenOptions::new()
            .write(true)
            .create(true)
            .open(file_name)?;

        let text_main = unsafe { context.element_by_name::<Text>("text_main") }
            .ok_or_else(|| Error::Custom("Failed tot find 'text_main' element".into()))?;
        let file_content = text_main.value()?;

        file.write_all(file_content.as_bytes())?;
        file.flush()?;
    }

    Ok(())
}

fn font_callback(
    context: &mut Context,
    _this: &mut MenuItem,
) -> Result<(), Box<dyn std::error::Error>> {
    use controls::Text;
    use dialogs::*;
    use element::private::ElementPrivate;
    use error::Error;

    let mut font_dialog = FontDialog::new()?;

    let text_main = unsafe { context.element_by_name::<Text>("text_main") }
        .ok_or_else(|| Error::Custom("Failed tot find 'text_main' element".into()))?;
    let font = text_main.font()?;
    font_dialog.set_value(font.into());

    font_dialog.set_popup()?;

    if font_dialog.status() != FontDialogStatus::Cancel {
        let font = font_dialog.value()?;
        text_main.set_font(font.into());
    }

    Ok(())
}

unsafe extern "C" fn about_callback(_this: *mut iup_sys::Ihandle) -> i32 {
    use dialogs::Message;
    use iup_sys::*;

    Message::show("About", "   Simple Notepad\n\nAuthors:\n   Romeo Calota");

    IUP_DEFAULT
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    use containers::*;
    use context::*;
    use controls::*;
    use dialogs::*;
    use element::*;
    use menus::*;
    use types::*;

    let mut context = Context::new();

    let text_main = Text::new()?
        .with_name("text_main")
        .multiline(true)
        .expand(true);

    let menu_item_open = MenuItem::new()?
        .with_title("Open")
        .with_action(|context, this| {
            open_callback(context, this).unwrap();
            CallbackResult::Default
        });
    let menu_item_save_as =
        MenuItem::new()?
            .with_title("Save As...")
            .with_action(|context, this| {
                save_as_callback(context, this).unwrap();
                CallbackResult::Default
            });
    let menu_item_exit = MenuItem::new()?
        .with_title("Exit")
        .with_action(|_, _| CallbackResult::Close);

    let file_menu = Menu::new()?
        .with_child(menu_item_open)?
        .with_child(menu_item_save_as)?
        .with_separator()?
        .with_child(menu_item_exit)?;
    let file_menu = SubMenu::new()?.with_title("File").with_menu(file_menu)?.0;

    let menu_item_font = MenuItem::new()?
        .with_title("Font...")
        .with_action(|context, this| {
            font_callback(context, this).unwrap();
            CallbackResult::Default
        });

    let format_menu = Menu::new()?.with_child(menu_item_font)?;
    let format_menu = SubMenu::new()?
        .with_title("Format")
        .with_menu(format_menu)?
        .0;

    let menu_item_about = MenuItem::new()?.with_title("About").with_action(|_, this| {
        use crate::element::private::ElementPrivate;
        unsafe { about_callback(this.handle_mut().into()) };
        CallbackResult::Default
    });

    let help_menu = Menu::new()?.with_child(menu_item_about)?;
    let help_menu = SubMenu::new()?.with_title("Help").with_menu(help_menu)?.0;

    let menu = Menu::new()?
        .with_child(file_menu)?
        .with_child(format_menu)?
        .with_child(help_menu)?;

    let layout = VBox::new()?.with_child(text_main)?;

    let mut main_window = Dialog::new()?
        .with_title("Simple Notepad")
        .with_menu(menu)?
        .0
        .with_child(layout)?
        .0;
    set_attribute_str!(&mut main_window, "SIZE", "QUARTERxQUARTER");

    context.run(main_window)?;

    Ok(())
}
