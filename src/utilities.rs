macro_rules! c_str {
    ($string: expr) => {{
        std::ffi::CString::new($string).unwrap()
    }};
}

macro_rules! c_str_literal {
    ($string: literal) => {{
        let cstring = concat!($string, "\0");
        cstring.as_ptr() as *const i8
    }};
}

macro_rules! va_args_end {
    () => {{
        core::ptr::null_mut() as *mut u8
    }};
}

macro_rules! set_attribute {
    ($ihandle: expr, $attribute: literal, $value: expr) => {{
        use $crate::element::private::ElementPrivate;
        let _handle: &mut dyn $crate::element::Element = ($ihandle);
        unsafe {
            $crate::iup_sys::IupSetAttribute(
                $ihandle.handle_mut().into(),
                c_str_literal!($attribute),
                ($value),
            )
        };
    }};
}

macro_rules! set_attribute_str {
    ($ihandle: expr, $attribute: literal, $value: literal) => {{
        set_attribute!($ihandle, $attribute, c_str_literal!($value));
    }};
}

// #[macro_export(local_inner_macros)]
macro_rules! set_attribute_handle {
    ($ihandle: expr, $attribute: literal, $value: expr) => {{
        use $crate::element::private::ElementPrivate;
        let _handle: &mut dyn $crate::element::Element = ($ihandle);
        let _handle: *mut $crate::iup_sys::Ihandle = ($value);
        unsafe {
            $crate::iup_sys::IupSetAttributeHandle(
                $ihandle.handle_mut().into(),
                c_str_literal!($attribute),
                ($value),
            )
        };
    }};
}
