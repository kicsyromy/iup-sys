mod menu;
mod menu_item;
mod sub_menu;

pub trait MenuElement<'a>: crate::element::Element<'a> {}

pub use menu::*;
pub use menu_item::*;
pub use sub_menu::*;
