use crate::context::Context;
use crate::types::CallbackResult;
use crate::{
    element::{private, Element, Handle},
    error::Error,
};

pub struct Menu<'a, 'c> {
    handle: Handle,
    children: Vec<Box<dyn Element<'a> + 'c>>,
}

impl<'a, 'c> Menu<'a, 'c> {
    pub fn new() -> Result<Self, Error> {
        use crate::{element::private::ElementPrivate, iup_sys::IupMenu};

        let native_handle = unsafe { IupMenu(std::ptr::null_mut(), va_args_end!()) };
        if native_handle == std::ptr::null_mut() {
            return Err(Error::Unknown(Self::class_name()));
        }

        Ok(Self {
            handle: Handle::from(native_handle),
            children: Vec::new(),
        })
    }

    pub fn push_separator(&mut self) -> Result<(), Error> {
        use crate::{
            element::private::ElementPrivate,
            iup_sys::{IupAppend, IupMap, IupSeparator},
        };

        let separator = unsafe { IupSeparator() };

        let parent = unsafe { IupAppend(self.handle_mut().into(), separator) };
        if parent == std::ptr::null_mut() {
            Err(Error::Unknown(Self::class_name()))
        } else {
            unsafe { IupMap(separator) };
            Ok(())
        }
    }

    pub fn push(&mut self, child: impl Element<'a> + 'c) -> Result<(), Error> {
        use crate::{
            element::private::ElementPrivate,
            iup_sys::{IupAppend, IupMap},
        };

        self.children.push(Box::new(child));
        let child_count = self.children.len();

        let child_handle = self.children[child_count - 1].handle_mut().iup_handle;

        let parent = unsafe { IupAppend(self.handle_mut().into(), child_handle) };
        let result = if parent == std::ptr::null_mut() {
            self.children.remove(child_count - 1);
            Err(Error::Unknown(Self::class_name()))
        } else {
            unsafe { IupMap(child_handle) };
            self.children[child_count - 1].update_self_reference();
            Ok(())
        };

        for child in &mut self.children {
            child.update_self_reference();
        }

        result
    }

    pub fn with_child(mut self, child: impl Element<'a> + 'c) -> Result<Self, Error> {
        self.push(child)?;
        Ok(self)
    }

    pub fn with_separator(mut self) -> Result<Self, Error> {
        self.push_separator()?;
        Ok(self)
    }
}

impl<'a, 'c> private::ElementPrivate<'a> for Menu<'a, 'c> {
    fn handle_mut(&mut self) -> &mut Handle {
        &mut self.handle
    }

    fn handle(&self) -> &Handle {
        &self.handle
    }

    fn class_name() -> &'static str {
        "Menu"
    }

    fn action_callback_pointer(
        &mut self,
    ) -> &mut Option<Box<dyn FnMut(&mut Context, &mut Self) -> CallbackResult + 'a>>
    where
        Self: Sized,
    {
        unimplemented!()
    }
}

impl<'a, 'c> Element<'a> for Menu<'a, 'c> {
    fn set_title(&mut self, _title: &str) {
        unimplemented!()
    }
}

impl<'a, 'c> super::MenuElement<'a> for Menu<'a, 'c> {}
