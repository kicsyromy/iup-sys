use std::ffi::CString;

use crate::context::Context;
use crate::types::CallbackResult;
use crate::{
    element::{private, Element, Handle},
    error::Error,
    menus::menu::Menu,
};

pub struct SubMenu<'a, 'm> {
    handle: Handle,
    menu: Option<Box<Menu<'a, 'm>>>,
    title: Option<CString>,
}

impl<'a, 'm> SubMenu<'a, 'm> {
    pub fn new() -> Result<Self, Error> {
        use crate::{element::private::ElementPrivate, iup_sys::IupSubmenu};

        let native_handle = unsafe { IupSubmenu(std::ptr::null(), std::ptr::null_mut()) };
        if native_handle == std::ptr::null_mut() {
            return Err(Error::Unknown(Self::class_name()));
        }

        Ok(Self {
            handle: Handle::from(native_handle),
            menu: None,
            title: None,
        })
    }

    pub fn set_menu(&mut self, menu: Menu<'a, 'm>) -> Result<Option<Menu<'a, 'm>>, Error> {
        use crate::{
            element::private::ElementPrivate,
            iup_sys::{IupAppend, IupDetach},
        };

        let orphan = if self.menu.is_some() {
            let mut old_menu: Menu<'a, 'm> = Menu::<'a, 'm>::new()?;
            std::mem::swap(&mut old_menu, self.menu.as_mut().unwrap().as_mut());

            let iup_handle = old_menu.handle_mut().iup_handle;
            unsafe { IupDetach(iup_handle) };
            Some(old_menu)
        } else {
            None
        };

        self.menu = Some(Box::new(menu));
        self.menu.as_mut().unwrap().update_self_reference();
        unsafe {
            IupAppend(
                self.handle_mut().into(),
                self.menu.as_mut().unwrap().handle_mut().into(),
            )
        };

        Ok(orphan)
    }

    pub fn with_title(mut self, title: &str) -> Self {
        self.set_title(title);
        self
    }

    pub fn with_menu(mut self, menu: Menu<'a, 'm>) -> Result<(Self, Option<Menu<'a, 'm>>), Error> {
        let old_menu = self.set_menu(menu)?;
        Ok((self, old_menu))
    }
}

impl<'a, 'm> private::ElementPrivate<'a> for SubMenu<'a, 'm> {
    fn handle_mut(&mut self) -> &mut Handle {
        &mut self.handle
    }

    fn handle(&self) -> &Handle {
        &self.handle
    }

    fn class_name() -> &'static str {
        "SubMenu"
    }

    fn action_callback_pointer(
        &mut self,
    ) -> &mut Option<Box<dyn FnMut(&mut Context, &mut Self) -> CallbackResult + 'a>>
    where
        Self: Sized,
    {
        unimplemented!()
    }
}

impl<'a, 'm> Element<'a> for SubMenu<'a, 'm> {
    fn set_title(&mut self, title: &str) {
        self.title = Some(c_str!(title));
        set_attribute!(self, "TITLE", self.title.as_ref().unwrap().as_ptr());
    }
}

impl<'a, 'm> super::MenuElement<'a> for SubMenu<'a, 'm> {}
