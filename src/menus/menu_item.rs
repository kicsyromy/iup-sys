use std::ffi::CString;

use crate::{
    context::Context,
    element::{private, Element, Handle},
    error::Error,
    types::CallbackResult,
};

pub struct MenuItem<'a> {
    handle: Handle,
    title: Option<CString>,
    action_cb: Option<Box<dyn FnMut(&mut Context, &mut Self) -> CallbackResult + 'a>>,
}

impl<'a> MenuItem<'a> {
    pub fn new() -> Result<Self, Error> {
        use crate::{element::private::ElementPrivate, iup_sys::IupItem};

        let native_handle = unsafe { IupItem(std::ptr::null(), std::ptr::null()) };
        if native_handle == std::ptr::null_mut() {
            return Err(Error::Unknown(Self::class_name()));
        }

        Ok(Self {
            handle: Handle::from(native_handle),
            action_cb: None,
            title: None,
        })
    }

    pub fn with_title(mut self, title: &str) -> Self {
        self.set_title(title);
        self
    }
}

impl<'a> private::ElementPrivate<'a> for MenuItem<'a> {
    fn handle_mut(&mut self) -> &mut Handle {
        &mut self.handle
    }

    fn handle(&self) -> &Handle {
        &self.handle
    }

    fn class_name() -> &'static str {
        "MenuItem"
    }

    fn action_callback_pointer(
        &mut self,
    ) -> &mut Option<Box<dyn FnMut(&mut Context, &mut Self) -> CallbackResult + 'a>>
    where
        Self: Sized,
    {
        &mut self.action_cb
    }
}

impl<'a> Element<'a> for MenuItem<'a> {
    fn set_title(&mut self, title: &str) {
        self.title = Some(c_str!(title));
        set_attribute!(self, "TITLE", self.title.as_ref().unwrap().as_ptr());
    }
}

impl<'a> super::MenuElement<'a> for MenuItem<'a> {}
