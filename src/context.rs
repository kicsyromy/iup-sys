use std::thread::ThreadId;

use spin::{Mutex, MutexGuard};

use crate::dialogs::Dialog;
use crate::element::Element;
use crate::error::Error;

static mut CONTEXT: Option<(Mutex<Context>, ThreadId)> = None;

pub struct Context {
    main_dialog: Option<Dialog<'static, 'static>>,
}

impl Context {
    pub fn new() -> MutexGuard<'static, Self> {
        use crate::iup_sys::{IupOpen, IupSetGlobal};

        assert!(unsafe { CONTEXT.is_none() });

        unsafe { IupSetGlobal(c_str_literal!("UTF8MODE"), c_str_literal!("YES")) };

        let args: Vec<String> = std::env::args().collect();

        let mut argc: i32 = args.len() as i32;

        let mut boxed_argv = Vec::new();
        for arg in args.iter().map(|arg| c_str!(arg.as_str())) {
            boxed_argv.push(arg);
        }
        let mut argv = Vec::new();
        for arg in boxed_argv.iter_mut().map(|arg| arg.as_ptr() as *mut i8) {
            argv.push(arg);
        }

        unsafe {
            IupOpen(
                &mut argc as *mut _,
                &mut argv.as_mut_ptr() as *mut *mut *mut i8,
            )
        };

        unsafe {
            CONTEXT = Some((
                Mutex::new(Self { main_dialog: None }),
                std::thread::current().id(),
            ));
            CONTEXT.as_mut().unwrap().0.lock()
        }
    }

    pub fn run(&mut self, main_dialog: Dialog<'static, 'static>) -> Result<(), Error> {
        use crate::{element::private::ElementPrivate, iup_sys::IupMainLoop};

        assert!(unsafe { CONTEXT.is_some() });

        let current_thread_id = std::thread::current().id();
        let context_thread_id = unsafe { &CONTEXT.as_mut().unwrap().1 };
        assert_eq!(current_thread_id, *context_thread_id);
        unsafe {
            CONTEXT.as_mut().unwrap().0.force_unlock();
        }

        self.main_dialog = Some(main_dialog);
        self.main_dialog.as_mut().unwrap().update_self_reference();
        self.main_dialog.as_mut().unwrap().show()?;

        unsafe { IupMainLoop() };
        Ok(())
    }

    pub unsafe fn element_by_name<'a, T: Element<'a> + Sized>(
        &mut self,
        id: &str,
    ) -> Option<&'a mut T> {
        use crate::iup_sys::{IupGetAttribute, IupGetHandle};

        let id = c_str!(id);

        let iup_handle = IupGetHandle(id.as_ptr());
        let this: *mut T =
            std::mem::transmute(IupGetAttribute(iup_handle, c_str_literal!("iup_rs_self")));

        if !this.is_null() {
            Some(&mut *this)
        } else {
            None
        }
    }

    pub(crate) fn instance() -> MutexGuard<'static, Self> {
        assert!(unsafe { CONTEXT.is_some() });
        unsafe { CONTEXT.as_mut().unwrap().0.lock() }
    }
}

impl Drop for Context {
    fn drop(&mut self) {
        use crate::iup_sys::IupClose;

        unsafe { IupClose() };
    }
}
