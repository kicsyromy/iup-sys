use std::{ffi::CString, path::PathBuf};

use crate::context::Context;
use crate::types::CallbackResult;
use crate::{
    element::{private, Element, Handle},
    error::Error,
};

const DIALOG_TYPE: [&'static [u8]; 3] = [b"OPEN\0", b"SAVE\0", b"DIR\0"];

#[repr(u8)]
pub enum FileDialogType {
    Open,
    Save,
    Directory,
}

#[repr(i8)]
#[derive(Eq, PartialEq)]
pub enum FileDialogStatus {
    NewFile = 1,
    ExistingFileOrDirectory = 0,
    OperationCancelled = -1,
}

impl From<i32> for FileDialogStatus {
    fn from(value: i32) -> Self {
        match value {
            1 => FileDialogStatus::NewFile,
            0 => FileDialogStatus::ExistingFileOrDirectory,
            -1 => FileDialogStatus::OperationCancelled,
            _ => panic!("Invalid status for file dialog"),
        }
    }
}

pub struct FileDialog<'a, 'c> {
    handle: Handle,
    title: Option<CString>,
    ext_filter: Option<CString>,
    _marker1: std::marker::PhantomData<&'a Self>,
    _marker2: std::marker::PhantomData<&'c Self>,
}

impl<'a, 'c> FileDialog<'a, 'c> {
    pub fn new() -> Result<Self, Error> {
        use crate::{element::private::ElementPrivate, iup_sys::IupFileDlg};

        let native_handle = unsafe { IupFileDlg() };
        if native_handle == std::ptr::null_mut() {
            return Err(Error::HandleAllocationFailed(Self::class_name()));
        }

        Ok(Self {
            handle: Handle::from(native_handle),
            title: None,
            ext_filter: None,
            _marker1: std::marker::PhantomData,
            _marker2: std::marker::PhantomData,
        })
    }

    pub fn with_title(mut self, title: &str) -> Self {
        self.set_title(title);
        self
    }

    pub fn set_type(&mut self, r#type: FileDialogType) {
        set_attribute!(
            self,
            "DIALOGTYPE",
            DIALOG_TYPE[r#type as usize].as_ptr() as *const i8
        );
    }

    pub fn with_type(mut self, r#type: FileDialogType) -> Self {
        self.set_type(r#type);
        self
    }

    pub fn set_extension_filters(&mut self, filters: &str) {
        self.ext_filter = Some(c_str!(filters));
        set_attribute!(
            self,
            "EXTFILTER",
            self.ext_filter.as_ref().unwrap().as_ptr()
        );
    }

    pub fn with_extension_filters(mut self, filters: &str) -> Self {
        self.set_extension_filters(filters);
        self
    }

    pub fn set_popup(&mut self) -> Result<(), Error> {
        use crate::{
            element::private::ElementPrivate,
            iup_sys::{IupPopup, IUP_CENTERPARENT, IUP_NOERROR},
        };

        if unsafe { IupPopup(self.handle_mut().into(), IUP_CENTERPARENT, IUP_CENTERPARENT) }
            == IUP_NOERROR
        {
            Ok(())
        } else {
            Err(Error::PopupFailed(Self::class_name()))
        }
    }

    pub fn popup(mut self) -> Result<Self, Error> {
        self.set_popup()?;
        Ok(self)
    }

    pub fn status(&self) -> FileDialogStatus {
        use crate::{element::private::ElementPrivate, iup_sys::IupGetInt};
        FileDialogStatus::from(unsafe {
            IupGetInt(self.handle().iup_handle, c_str_literal!("STATUS"))
        })
    }

    pub fn value(&self) -> Result<Vec<PathBuf>, Error> {
        use std::ffi::CStr;

        use crate::{element::private::ElementPrivate, iup_sys::IupGetAttribute};

        let file_names_native =
            unsafe { IupGetAttribute(self.handle.iup_handle, c_str_literal!("VALUE")) };
        let file_names = unsafe {
            CStr::from_ptr(file_names_native)
                .to_str()
                .map_err(|_| Error::Utf8Error(Self::class_name()))?
        };

        let mut directory = "";
        let mut files = Vec::new();
        for (index, entry) in file_names.split('|').enumerate() {
            if index == 0 {
                directory = entry;
                continue;
            }

            files.push([directory, entry].iter().collect());
        }

        if files.is_empty() {
            files.push(PathBuf::from(directory));
        }

        Ok(files)
    }
}

impl<'a, 'c> private::ElementPrivate<'a> for FileDialog<'a, 'c> {
    fn handle_mut(&mut self) -> &mut Handle {
        &mut self.handle
    }

    fn handle(&self) -> &Handle {
        &self.handle
    }

    fn class_name() -> &'static str {
        "FileDialog"
    }

    fn action_callback_pointer(
        &mut self,
    ) -> &mut Option<Box<dyn FnMut(&mut Context, &mut Self) -> CallbackResult + 'a>>
    where
        Self: Sized,
    {
        unimplemented!()
    }
}

impl<'a, 'c> Element<'a> for FileDialog<'a, 'c> {
    fn set_title(&mut self, title: &str) {
        self.title = Some(c_str!(title));
        set_attribute!(self, "TITLE", self.title.as_ref().unwrap().as_ptr());
    }
}

impl<'a, 'c> super::DialogElement<'a> for FileDialog<'a, 'c> {}
