use std::ffi::CString;

use crate::context::Context;
use crate::types::CallbackResult;
use crate::{
    element::{private, Element, Handle},
    error::Error,
    menus::Menu,
};

pub struct Dialog<'a, 'c> {
    handle: Handle,
    menu: Option<Box<Menu<'a, 'c>>>,
    child: Option<Box<dyn Element<'a> + 'c>>,
    title: Option<CString>,
}

impl<'a, 'c> Dialog<'a, 'c> {
    pub fn new() -> Result<Self, Error> {
        use crate::{element::private::ElementPrivate, iup_sys::IupDialog};

        let native_handle = unsafe { IupDialog(core::ptr::null_mut()) };
        if native_handle == std::ptr::null_mut() {
            return Err(Error::HandleAllocationFailed(Self::class_name()));
        }

        Ok(Self {
            handle: Handle::from(native_handle),
            menu: None,
            child: None,
            title: None,
        })
    }

    pub fn set_child(
        &mut self,
        mut child: impl Element<'a> + 'c,
    ) -> Result<Option<Box<dyn Element<'a> + 'c>>, Error> {
        use crate::{
            element::private::ElementPrivate,
            iup_sys::{IupAppend, IupDetach},
        };

        let new_child_handle = child.handle_mut().iup_handle;

        let orphan = if self.child.is_some() {
            let mut old_child: Option<Box<dyn crate::element::Element<'a> + 'c>> =
                Some(Box::new(child));

            std::mem::swap(&mut old_child, &mut self.child);
            unsafe { IupDetach(old_child.as_mut().unwrap().handle_mut().into()) };

            old_child
        } else {
            self.child = Some(Box::new(child));
            unsafe { IupAppend(self.handle_mut().into(), new_child_handle) };
            None
        };

        Ok(orphan)
    }

    pub fn set_menu(&mut self, menu: Menu<'a, 'c>) -> Result<Option<Menu<'a, 'c>>, Error> {
        use crate::{
            element::private::ElementPrivate,
            iup_sys::{IupDetach, IupSetAttributeHandle},
        };

        let orphan = if self.menu.is_some() {
            let mut old_menu: Menu<'a, 'c> = Menu::<'a, 'c>::new()?;
            std::mem::swap(&mut old_menu, self.menu.as_mut().unwrap().as_mut());

            let iup_handle = old_menu.handle_mut().iup_handle;
            unsafe { IupDetach(iup_handle) };
            Some(old_menu)
        } else {
            None
        };

        self.menu = Some(Box::new(menu));
        self.menu.as_mut().unwrap().update_self_reference();
        unsafe {
            IupSetAttributeHandle(
                self.handle_mut().into(),
                c_str_literal!("MENU"),
                self.menu.as_mut().unwrap().handle_mut().into(),
            )
        };

        Ok(orphan)
    }

    pub fn show(&mut self) -> Result<(), Error> {
        use crate::{
            element::private::ElementPrivate,
            iup_sys::{IupShow, IUP_NOERROR},
        };

        if unsafe { IupShow(self.handle_mut().into()) } == IUP_NOERROR {
            Ok(())
        } else {
            Err(Error::Unknown(Self::class_name()))
        }
    }

    pub fn with_title(mut self, title: &str) -> Self {
        self.set_title(title);
        self
    }

    pub fn with_menu(mut self, menu: Menu<'a, 'c>) -> Result<(Self, Option<Menu<'a, 'c>>), Error> {
        let old_menu = self.set_menu(menu)?;
        Ok((self, old_menu))
    }

    pub fn with_child(
        mut self,
        child: impl Element<'a> + 'c,
    ) -> Result<(Self, Option<Box<dyn Element<'a> + 'c>>), Error> {
        let old_child = self.set_child(child)?;
        Ok((self, old_child))
    }
}

impl<'a, 'c> private::ElementPrivate<'a> for Dialog<'a, 'c> {
    fn handle_mut(&mut self) -> &mut Handle {
        &mut self.handle
    }

    fn handle(&self) -> &Handle {
        &self.handle
    }

    fn class_name() -> &'static str {
        "Dialog"
    }

    fn action_callback_pointer(
        &mut self,
    ) -> &mut Option<Box<dyn FnMut(&mut Context, &mut Self) -> CallbackResult + 'a>>
    where
        Self: Sized,
    {
        unimplemented!()
    }
}

impl<'a, 'c> Element<'a> for Dialog<'a, 'c> {
    fn set_title(&mut self, title: &str) {
        self.title = Some(c_str!(title));
        set_attribute!(self, "TITLE", self.title.as_ref().unwrap().as_ptr());
    }
}

impl<'a, 'c> super::DialogElement<'a> for Dialog<'a, 'c> {}
