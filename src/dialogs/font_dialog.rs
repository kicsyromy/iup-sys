use std::ffi::CString;

use crate::context::Context;
use crate::types::CallbackResult;
use crate::{
    element::{private, Element, Handle},
    error::Error,
};

#[repr(i8)]
#[derive(Eq, PartialEq)]
pub enum FontDialogStatus {
    Ok = 1,
    Cancel = 0,
}

impl From<i32> for FontDialogStatus {
    fn from(value: i32) -> Self {
        match value {
            1 => FontDialogStatus::Ok,
            0 => FontDialogStatus::Cancel,
            _ => panic!("Invalid status for font dialog"),
        }
    }
}

// FIXME: The lifetime for the VALUE property is not correct
//        A situation could arise where the value it is
//        retrieved and subsequently changed, and the compiler would
//        not pick up on this based on the lifetime parameters
pub struct FontDialog<'a, 'c> {
    handle: Handle,
    title: Option<CString>,
    _marker1: std::marker::PhantomData<&'a Self>,
    _marker2: std::marker::PhantomData<&'c Self>,
}

impl<'a, 'c> FontDialog<'a, 'c> {
    pub fn new() -> Result<Self, Error> {
        use crate::{element::private::ElementPrivate, iup_sys::IupFontDlg};

        let native_handle = unsafe { IupFontDlg() };
        if native_handle.is_null() {
            return Err(Error::HandleAllocationFailed(Self::class_name()));
        }

        Ok(Self {
            handle: Handle::from(native_handle),
            title: None,
            _marker1: std::marker::PhantomData,
            _marker2: std::marker::PhantomData,
        })
    }

    pub fn with_title(mut self, title: &str) -> Self {
        self.set_title(title);
        self
    }

    pub fn set_popup(&mut self) -> Result<(), Error> {
        use crate::{
            element::private::ElementPrivate,
            iup_sys::{IupPopup, IUP_CENTERPARENT, IUP_NOERROR},
        };

        if unsafe { IupPopup(self.handle_mut().into(), IUP_CENTERPARENT, IUP_CENTERPARENT) }
            == IUP_NOERROR
        {
            Ok(())
        } else {
            Err(Error::PopupFailed(Self::class_name()))
        }
    }

    pub fn popup(mut self) -> Result<Self, Error> {
        self.set_popup()?;
        Ok(self)
    }

    pub fn status(&self) -> FontDialogStatus {
        use crate::{element::private::ElementPrivate, iup_sys::IupGetInt};
        FontDialogStatus::from(unsafe {
            IupGetInt(self.handle().iup_handle, c_str_literal!("STATUS"))
        })
    }

    pub fn value(&self) -> Result<&'a str, Error> {
        use std::ffi::CStr;

        use crate::{element::private::ElementPrivate, iup_sys::IupGetAttribute};

        Ok(unsafe {
            CStr::from_ptr(IupGetAttribute(
                self.handle().iup_handle,
                c_str_literal!("VALUE"),
            ))
        }
        .to_str()
        .map_err(|_| Error::Utf8Error(Self::class_name()))?)
    }

    pub fn set_value(&mut self, mut font: String) {
        use crate::element::private::ElementPrivate;
        use crate::iup_sys::IupSetStrAttribute;

        if !font.ends_with('\0') {
            font.push('\0');
        }

        unsafe {
            IupSetStrAttribute(
                self.handle_mut().into(),
                c_str_literal!("VALUE"),
                font.as_ptr() as *const i8,
            )
        }
    }
}

impl<'a, 'c> private::ElementPrivate<'a> for FontDialog<'a, 'c> {
    fn handle_mut(&mut self) -> &mut Handle {
        &mut self.handle
    }

    fn handle(&self) -> &Handle {
        &self.handle
    }

    fn class_name() -> &'static str {
        "FontDialog"
    }

    fn action_callback_pointer(
        &mut self,
    ) -> &mut Option<Box<dyn FnMut(&mut Context, &mut Self) -> CallbackResult + 'a>>
    where
        Self: Sized,
    {
        unimplemented!()
    }
}

impl<'a, 'c> Element<'a> for FontDialog<'a, 'c> {
    fn set_title(&mut self, title: &str) {
        self.title = Some(c_str!(title));
        set_attribute!(self, "TITLE", self.title.as_ref().unwrap().as_ptr());
    }
}

impl<'a, 'c> super::DialogElement<'a> for FontDialog<'a, 'c> {}
