pub struct Message {}

impl Message {
    pub fn show(title: &str, message: &str) {
        use crate::iup_sys::IupMessage;

        unsafe { IupMessage(c_str!(title).as_ptr(), c_str!(message).as_ptr()) };
    }
}
