mod dialog;
mod file_dialog;
mod font_dialog;
mod message;

pub use dialog::*;
pub use file_dialog::*;
pub use font_dialog::*;
pub use message::*;

pub trait DialogElement<'a>: crate::element::Element<'a> {}
