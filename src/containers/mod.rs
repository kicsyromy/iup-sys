mod vbox;

pub trait ContainerElement<'a>: crate::element::Element<'a> {}

pub use vbox::*;
