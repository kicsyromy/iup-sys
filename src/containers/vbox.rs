use crate::context::Context;
use crate::types::CallbackResult;
use crate::{
    element::{private, Element, Handle},
    error::Error,
};

pub struct VBox<'a, 'c> {
    handle: Handle,
    children: Vec<Box<dyn Element<'a> + 'c>>,
}

impl<'a, 'c> VBox<'a, 'c> {
    pub fn new() -> Result<Self, Error> {
        use crate::{element::private::ElementPrivate, iup_sys::IupVbox};

        let native_handle = unsafe { IupVbox(std::ptr::null_mut(), va_args_end!()) };
        if native_handle == std::ptr::null_mut() {
            return Err(Error::HandleAllocationFailed(Self::class_name()));
        }

        Ok(Self {
            handle: Handle::from(native_handle),
            children: Vec::new(),
        })
    }

    pub fn push(&mut self, child: impl Element<'a> + 'c) -> Result<(), Error> {
        use crate::{
            element::private::ElementPrivate,
            iup_sys::{IupAppend, IupMap},
        };

        self.children.push(Box::new(child));
        let child_count = self.children.len();

        let child_handle = self.children[child_count - 1]
            .as_mut()
            .handle_mut()
            .iup_handle;

        let parent = unsafe { IupAppend(self.handle_mut().into(), child_handle) };
        let result = if parent == std::ptr::null_mut() {
            self.children.remove(child_count - 1);
            Err(Error::AppendFailed(Self::class_name()))
        } else {
            unsafe { IupMap(child_handle) };
            self.children[child_count - 1].update_self_reference();
            Ok(())
        };

        for child in &mut self.children {
            child.update_self_reference();
        }

        result
    }

    pub fn with_child(mut self, child: impl Element<'a> + 'c) -> Result<Self, Error> {
        self.push(child)?;
        Ok(self)
    }
}

impl<'a, 'c> private::ElementPrivate<'a> for VBox<'a, 'c> {
    fn handle_mut(&mut self) -> &mut Handle {
        &mut self.handle
    }

    fn handle(&self) -> &Handle {
        &self.handle
    }

    fn class_name() -> &'static str {
        "VBox"
    }

    fn action_callback_pointer(
        &mut self,
    ) -> &mut Option<Box<dyn FnMut(&mut Context, &mut Self) -> CallbackResult + 'a>>
    where
        Self: Sized,
    {
        unimplemented!()
    }
}

impl<'a, 'c> Element<'a> for VBox<'a, 'c> {
    fn set_title(&mut self, _title: &str) {}
}

impl<'a, 'c> super::ContainerElement<'a> for VBox<'a, 'c> {}
