use crate::dependency::Dependency;

pub trait Platform {
    fn name(&self) -> &'static str;
    fn make_command_line(&self) -> String;
    fn uname(&self) -> String;
    fn environment(&self) -> Vec<(String, String)>;
    fn dependencies(&self, output_path: &str) -> Vec<Dependency>;

    fn run_command(
        command_line: &str,
        working_directory: &str,
    ) -> Result<String, Box<dyn std::error::Error>> {
        use std::{io::Read, process::*};

        let command_line: Vec<&str> = command_line.split_whitespace().collect();

        let mut command = Command::new(&command_line[0]);
        command
            .stdout(Stdio::piped())
            .current_dir(working_directory);
        for arg in &command_line[1..] {
            command.arg(*arg);
        }

        let mut command = command.spawn()?;

        let status = command.wait()?.code().unwrap();
        if status != 0 {
            return Err(format!("Failed to run command: {:?}", command_line).into());
        }

        let mut stdout_buffer = Vec::new();
        let stdout = command.stdout.as_mut().unwrap();
        stdout.read_to_end(&mut stdout_buffer)?;

        Ok(
            unsafe { core::str::from_utf8_unchecked(stdout_buffer.as_slice()) }
                .trim()
                .to_string(),
        )
    }
}
