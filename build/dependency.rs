use std::path::PathBuf;

pub struct Dependency {
    pub libs: Vec<String>,
    pub link_paths: Vec<PathBuf>,
    pub frameworks: Vec<String>,
    pub framework_paths: Vec<PathBuf>,
    pub r#static: bool,
}

impl Dependency {
    pub fn emit_link_options(&self) -> Result<(), Box<dyn std::error::Error>> {
        for path in self.link_paths.iter().map(|p| p.to_str().unwrap()) {
            println!("cargo:rustc-link-search=native={}", path);
        }

        if self.r#static {
            for lib in self.libs.iter() {
                println!("cargo:rustc-link-lib=static={}", lib);
            }
        } else {
            for lib in self.libs.iter() {
                println!("cargo:rustc-link-lib={}", lib);
            }
        }

        Ok(())
    }
}
