use crate::{dependency::Dependency, platform::Platform};

pub struct PlatformWindows {
    vs_install_path: String,
    vs_version: i32,
    win_sdk_path: String,
}

impl PlatformWindows {
    pub fn new() -> Result<Self, Box<dyn std::error::Error>> {
        use regex::Regex;
        use std::env;

        let tools_path = env::var("CARGO_MANIFEST_DIR").unwrap() + "\\tools";

        let vs_install_path = Self::run_command("cmd /c findvc.bat", &tools_path)?;
        let win_sdk_path = Self::run_command("cmd /c findwinsdk.bat", &tools_path)?;

        let mut vs_version = 0;
        let re = Regex::new("20[0-9][0-9]$")?;
        for dir in vs_install_path.split('\\').map(|s| s.trim()) {
            if !re.is_match(dir) {
                continue;
            }

            let version = Self::get_vs_version(dir);
            if version.is_none() {
                return Err(
                    "Failed to to find a valid Visual Studio installation: Unknown version"
                        .to_string()
                        .into(),
                );
            }

            vs_version = version.unwrap();
            break;
        }

        Ok(Self {
            vs_install_path,
            vs_version,
            win_sdk_path,
        })
    }

    fn get_vs_version(year: &str) -> Option<i32> {
        match year {
            "2019" => Some(16),
            "2017" => Some(15),
            "2015" => Some(14),
            _ => None,
        }
    }
}

impl Platform for PlatformWindows {
    fn name(&self) -> &'static str {
        "windows"
    }

    fn make_command_line(&self) -> String {
        "busybox.exe sh -c \"make\"".to_string()
    }

    #[cfg(target_pointer_width = "64")]
    fn uname(&self) -> String {
        format!("vc{}_64", self.vs_version)
    }

    #[cfg(target_pointer_width = "32")]
    fn uname(&self) -> String {
        format!("vc{}_32", self.vs_version)
    }

    fn environment(&self) -> Vec<(String, String)> {
        use std::env;

        let path = env::var("PATH").unwrap();
        let tools_path = env::var("CARGO_MANIFEST_DIR").unwrap() + "\\tools";

        let mut res = Vec::new();
        res.reserve(3);
        res.push(("TEC_UNAME".to_string(), self.uname()));
        res.push((
            format!("VC{}", self.vs_version),
            self.vs_install_path.clone(),
        ));
        res.push((
            format!("VC{}SDK", self.vs_version),
            self.win_sdk_path.clone(),
        ));
        res.push(("PATH".to_string(), format!("{};{}", tools_path, path)));

        res
    }

    // TODO: Kinda hardcoded some suff here, need to extract this info from the compiled packages
    fn dependencies(&self, output_path: &str) -> Vec<Dependency> {
        let iup_lib_path = format!("{}/iup/lib/{}", output_path.to_string(), self.uname());
        let cd_lib_path = format!("{}/cd/lib/{}", output_path.to_string(), self.uname());
        let im_lib_path = format!("{}/im/lib/{}", output_path.to_string(), self.uname());
        let freetype_lib_path =
            format!("{}/freetype/lib/{}", output_path.to_string(), self.uname());
        let zlib_lib_path = format!("{}/zlib/lib/{}", output_path.to_string(), self.uname());
        let sys_lib_path = "C:\\Windows\\System32";

        let mut dependencies = Vec::new();

        dependencies.push(Dependency {
            libs: vec![
                "iupimglib".to_string(),
                "iupcd".into(),
                "iupole".into(),
                "iupfiledlg".into(),
                "iupweb".into(),
                "iup".into(),
            ],
            link_paths: vec![std::path::PathBuf::from(iup_lib_path)],
            frameworks: vec![],
            framework_paths: vec![],
            r#static: true,
        });

        dependencies.push(Dependency {
            libs: vec!["cd".to_string()],
            link_paths: vec![std::path::PathBuf::from(cd_lib_path)],
            frameworks: vec![],
            framework_paths: vec![],
            r#static: true,
        });

        dependencies.push(Dependency {
            libs: vec!["im_process".to_string(), "im".into()],
            link_paths: vec![std::path::PathBuf::from(im_lib_path)],
            frameworks: vec![],
            framework_paths: vec![],
            r#static: true,
        });

        dependencies.push(Dependency {
            libs: vec!["freetype6".to_string()],
            link_paths: vec![std::path::PathBuf::from(freetype_lib_path)],
            frameworks: vec![],
            framework_paths: vec![],
            r#static: true,
        });

        dependencies.push(Dependency {
            libs: vec!["zlib1".to_string()],
            link_paths: vec![std::path::PathBuf::from(zlib_lib_path)],
            frameworks: vec![],
            framework_paths: vec![],
            r#static: true,
        });

        dependencies.push(Dependency {
            libs: vec![
                "opengl32".to_string(),
                "kernel32".into(),
                "user32".into(),
                "gdi32".into(),
                "comdlg32".into(),
                "comctl32".into(),
                "shell32".into(),
                "ole32".into(),
                "advapi32".into(),
                "WinSpool".into(),
            ],
            link_paths: vec![std::path::PathBuf::from(sys_lib_path)],
            frameworks: vec![],
            framework_paths: vec![],
            r#static: false,
        });

        #[cfg(target_pointer_width = "64")]
        let atlmfc_path = self.vs_install_path.clone() + "\\atlmfc\\lib\\x64";
        #[cfg(target_pointer_width = "32")]
        let atlmfc_path = self.vs_install_path.clone() + "\\atlmfc\\lib\\x32";

        dependencies.push(Dependency {
            libs: vec!["atls".to_string()],
            link_paths: vec![std::path::PathBuf::from(atlmfc_path)],
            frameworks: vec![],
            framework_paths: vec![],
            r#static: false,
        });

        dependencies
    }
}
