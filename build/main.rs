mod dependency;
mod package;
mod platform;

#[cfg(target_os = "windows")]
mod platform_windows;

#[cfg(target_os = "linux")]
mod platform_linux;

use hex_literal::hex;

static mut PACKAGES: &mut [package::Package] = &mut [
    pkg!(
        "pdflib",
        "7.0.5",
        hex!("cecca665d5c17e410dc3a1f29bd26883b7905ceab171b1b2a8f5f57a31b51ba5"),
        "canvasdraw",
        "5.14",
        "_lite"
    ),
    pkg!(
        "ftgl",
        "2.1.5",
        hex!("ea50be16ec0261d2cf870f0114db0537a2f32c0078be819cc0f18b1385bd483f"),
        "canvasdraw",
        "5.14",
        ""
    ),
    pkg!(
        "im",
        "3.15",
        hex!("80dc672f415862154e7c4a8bfbf52f97b601eb4f4461d2669d270fae8f44b7b0"),
        "imtoolkit",
        "3.15",
        ""
    ),
    pkg!(
        "cd",
        "5.14",
        hex!("77bd4a66c54b46f6c3adde40768fc47e46cb42b5d255de69958b85e1bf718bae"),
        "canvasdraw",
        "5.14",
        ""
    ),
    pkg!(
        "iup",
        "3.30",
        hex!("ed878b263f8c0057a0fa67c6e34ff958642d94a2a9569a7afa7a1f864bb11a0d"),
        "iup",
        "3.30",
        ""
    ),
];

#[cfg(target_os = "windows")]
static mut EXTRA_PACKAGES: &mut [package::Package] = &mut [
    pkg!(
        "zlib",
        "1.2.11",
        hex!("8bd6b58fdba74ba7f7b988d22d1ffef43d5582a6d20cea9a33b1ff02dbfececc"),
        "canvasdraw",
        "5.14",
        ""
    ),
    pkg!(
        "freetype",
        "2.10.2",
        hex!("7cf9044e7d2d3bacf46989c96008b2cc78d3cbb471031f60a7b5a9ca24dcd22c"),
        "canvasdraw",
        "5.14",
        ""
    ),
];

#[cfg(target_os = "linux")]
static mut EXTRA_PACKAGES: &mut [package::Package] = &mut [package::Package::empty(); 0];

fn main() -> Result<(), Box<dyn std::error::Error>> {
    use bindgen::MacroTypeVariation;
    use platform::Platform;
    use std::env;

    let packages = unsafe { &mut PACKAGES };
    let extra_packages = unsafe { &mut EXTRA_PACKAGES };

    let output_path = env::var("OUT_DIR").unwrap();
    let src_path = env::var("CARGO_MANIFEST_DIR").unwrap() + "/src";

    #[cfg(target_os = "windows")]
    let platform = platform_windows::PlatformWindows::new()?;

    #[cfg(target_os = "windows")]
    embed_resource::compile("iup-sys.exe.rc");

    #[cfg(target_os = "linux")]
    let platform = platform_linux::PlatformLinux::new()?;

    if platform.name() == "windows" {
        packages[2].make_cmd_override =
            Some("busybox.exe sh -c \"make -C ./src/ im im_jp2 im_avi im_process im_process_omp\"");
        packages[3].make_cmd_override =
            Some("busybox.exe sh -c \"make -C ./src/ cd cdpdf cdgl cdim cdcontextplus\"");
    } else if platform.name() == "linux" {
        packages[2].make_cmd_override = Some("make -C ./src/ im im_jp2 im_process im_process_omp");
        packages[3].make_cmd_override = Some("make -C ./src/ cd cdpdf cdgl cdim cdcontextplus");
    }

    for pkg in extra_packages.iter() {
        pkg.download(&output_path)?;
        let dir = pkg.unpack(&output_path)?;
        let package_folder = output_path.clone() + "/" + &dir;
        pkg.build(&package_folder, &platform)?;
    }

    for pkg in packages.iter() {
        pkg.download(&output_path)?;
        let dir = pkg.unpack(&output_path)?;

        let package_folder = output_path.clone() + "/" + &dir;
        pkg.build(&package_folder, &platform)?;
    }

    for dependency in &platform.dependencies(&output_path) {
        dependency.emit_link_options()?;
    }

    let iup_include_dir = output_path.to_string() + "/" + packages[4].name.clone() + "/include";

    #[cfg(target_os = "windows")]
    {
        let tools_path = env::var("CARGO_MANIFEST_DIR").unwrap() + "\\tools\\";

        #[cfg(target_pointer_width = "64")]
        let clang_dll = tools_path + "libclang64.dll";
        #[cfg(target_pointer_width = "32")]
        let clang_dll = tools_path + "libclang32.dll";

        std::fs::copy(clang_dll, output_path.clone() + "\\libclang.dll")?;

        env::set_var("LIBCLANG_PATH", &output_path);
    }

    let bindings = bindgen::Builder::default()
        .use_core()
        .header(format!("{}/iup-sys.h", src_path))
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .layout_tests(false)
        .default_macro_constant_type(MacroTypeVariation::Signed)
        .fit_macro_constants(false)
        .clang_arg(format!("-I{}", iup_include_dir))
        .generate()
        .expect("Unable to generate bindings");

    bindings
        .write_to_file(std::path::PathBuf::from(output_path).join("bindings.rs"))
        .expect("Couldn't write bindings!");

    Ok(())
}
