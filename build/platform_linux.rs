use crate::{dependency::Dependency, platform::Platform};

pub struct PlatformLinux {
    uname: String,
}

impl PlatformLinux {
    pub fn new() -> Result<Self, Box<dyn std::error::Error>> {
        use std::env;

        let tools_path = env::var("CARGO_MANIFEST_DIR").unwrap() + "/tools";
        let uname = Self::run_command("sh ./tec-uname.sh", &tools_path)?;

        Ok(Self { uname })
    }
}

impl Platform for PlatformLinux {
    fn name(&self) -> &'static str {
        "linux"
    }

    fn make_command_line(&self) -> String {
        "make".to_string()
    }

    fn uname(&self) -> String {
        self.uname.clone()
    }

    fn environment(&self) -> Vec<(String, String)> {
        vec![]
    }

    // TODO: Kinda hardcoded some suff here, need to extract this info from the compiled packages
    fn dependencies(&self, output_path: &str) -> Vec<Dependency> {
        let iup_lib_path = format!("{}/iup/lib/{}", output_path.to_string(), self.uname());
        let cd_lib_path = format!("{}/cd/lib/{}", output_path.to_string(), self.uname());
        let im_lib_path = format!("{}/im/lib/{}", output_path.to_string(), self.uname());

        let mut dependencies = Vec::new();

        dependencies.push(Dependency {
            libs: vec![
                "iupimglib".to_string(),
                "iupcd".into(),
                "iupweb".into(),
                "iup".into(),
            ],
            link_paths: vec![std::path::PathBuf::from(iup_lib_path)],
            frameworks: vec![],
            framework_paths: vec![],
            r#static: true,
        });

        dependencies.push(Dependency {
            libs: vec!["cd".to_string()],
            link_paths: vec![std::path::PathBuf::from(cd_lib_path)],
            frameworks: vec![],
            framework_paths: vec![],
            r#static: true,
        });

        dependencies.push(Dependency {
            libs: vec!["im_process".to_string(), "im".into()],
            link_paths: vec![std::path::PathBuf::from(im_lib_path)],
            frameworks: vec![],
            framework_paths: vec![],
            r#static: true,
        });

        dependencies.push({
            let lib = pkg_config::probe_library("gtk+-3.0")
                .expect("Failed to find GTK+3.0 development libraries");
            Dependency {
                libs: lib.libs,
                link_paths: lib.link_paths,
                frameworks: lib.frameworks,
                framework_paths: lib.framework_paths,
                r#static: false,
            }
        });

        dependencies.push({
            let lib = pkg_config::probe_library("webkit2gtk-4.0")
                .expect("Failed to find GTK+ Webkit 4.0 development libraries");
            Dependency {
                libs: lib.libs,
                link_paths: lib.link_paths,
                frameworks: lib.frameworks,
                framework_paths: lib.framework_paths,
                r#static: false,
            }
        });

        dependencies.push({
            let lib = pkg_config::probe_library("gmodule-2.0")
                .expect("Failed to find GModule development libraries");
            Dependency {
                libs: lib.libs,
                link_paths: lib.link_paths,
                frameworks: lib.frameworks,
                framework_paths: lib.framework_paths,
                r#static: false,
            }
        });

        dependencies.push({
            let lib = pkg_config::probe_library("fontconfig")
                .expect("Failed to find fontconfig development libraries");
            Dependency {
                libs: lib.libs,
                link_paths: lib.link_paths,
                frameworks: lib.frameworks,
                framework_paths: lib.framework_paths,
                r#static: false,
            }
        });

        dependencies.push({
            let lib = pkg_config::probe_library("libpng")
                .expect("Failed to find libpng development libraries");
            Dependency {
                libs: lib.libs,
                link_paths: lib.link_paths,
                frameworks: lib.frameworks,
                framework_paths: lib.framework_paths,
                r#static: false,
            }
        });

        dependencies.push({
            let lib =
                pkg_config::probe_library("x11").expect("Failed to find X11 development libraries");
            Dependency {
                libs: lib.libs,
                link_paths: lib.link_paths,
                frameworks: lib.frameworks,
                framework_paths: lib.framework_paths,
                r#static: false,
            }
        });

        dependencies.push({
            let lib = pkg_config::probe_library("zlib")
                .expect("Failed to find zlib development libraries");
            Dependency {
                libs: lib.libs,
                link_paths: lib.link_paths,
                frameworks: lib.frameworks,
                framework_paths: lib.framework_paths,
                r#static: false,
            }
        });

        dependencies.push({
            Dependency {
                libs: vec!["stdc++".to_string()],
                link_paths: Vec::new(),
                frameworks: Vec::new(),
                framework_paths: Vec::new(),
                r#static: false,
            }
        });

        dependencies
    }
}
