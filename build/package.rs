#[macro_export]
macro_rules! pkg {
    ($name: literal, $version: literal, $sha256sum: expr, $base_project: literal, $project_version: literal, $name_suffix: literal) => {{
        use const_format::concatcp;
        $crate::package::Package {
            name: $name,
            version: $version,
            sha256sum: $sha256sum,
            download_url: concatcp!(
                "https://sourceforge.net/projects/",
                $base_project,
                "/files/",
                $project_version,
                "/Docs%20and%20Sources/",
                $name,
                $name_suffix,
                "-",
                $version,
                "_Sources.tar.gz/download"
            ),
            file_name: concatcp!($name, "-", $version, ".tar.gz"),
            make_cmd_override: None,
        }
    }};
}

use crate::platform::Platform;
use std::{error::Error, io::ErrorKind};

#[derive(Copy, Clone)]
pub struct Package {
    pub name: &'static str,
    pub version: &'static str,
    pub sha256sum: [u8; 32],
    pub download_url: &'static str,
    pub file_name: &'static str,
    pub make_cmd_override: Option<&'static str>,
}

impl Package {
    #[allow(dead_code)]
    pub const fn empty() -> Self {
        Self {
            name: "",
            version: "",
            sha256sum: [0; 32],
            download_url: "",
            file_name: "",
            make_cmd_override: None,
        }
    }

    fn check_local(&self, output_path: &str) -> Result<bool, Box<dyn Error>> {
        use sha2::Digest;
        use std::{fs::OpenOptions, io::Read};

        let mut file_content = Vec::new();
        let file = OpenOptions::new().read(true).open(output_path);
        match file {
            Ok(mut file) => {
                file.read_to_end(&mut file_content)?;
            }
            Err(err) => match err.kind() {
                ErrorKind::NotFound => {}
                _ => return Err(Box::new(err)),
            },
        };

        let mut hasher = sha2::Sha256::new();
        hasher.update(file_content.as_slice());
        let hash_value = hasher.finalize();

        Ok(hash_value.as_slice() == self.sha256sum)
    }

    pub fn download(&self, output_path: &str) -> Result<(), Box<dyn Error>> {
        use std::{
            fs::File,
            io::{Read, Write},
        };

        let output_path = output_path.to_string() + "/" + self.file_name;

        if self.check_local(&output_path)? {
            return Ok(());
        }

        let response = ureq::get(&self.download_url).call()?;
        if response.status() != 200 {
            eprintln!(
                "Failed to download package {} from {}. HTTP status code {} {}",
                self.name,
                self.download_url,
                response.status(),
                response.status_text()
            );
        }

        let mut content = Vec::new();
        response.into_reader().read_to_end(&mut content)?;

        let mut file = File::create(output_path)?;
        file.write_all(content.as_slice())?;
        file.flush()?;

        Ok(())
    }

    pub fn unpack(&self, output_path: &str) -> Result<String, Box<dyn Error>> {
        use archiver_rs::Compressed;
        use std::{
            fs::File,
            io::{Read, Write},
            path::{Path, PathBuf},
        };

        let tarball = output_path.to_string() + "/" + self.file_name;

        let test_file = tarball.trim_end_matches(".tar.gz").to_string() + ".unpacked";
        if let Ok(archive_unpacked) = std::fs::read(test_file.as_str()) {
            let archive_root_folder = unsafe { std::str::from_utf8_unchecked(&archive_unpacked) };
            return Ok(archive_root_folder.to_string());
        }

        let tar_file = tarball.trim_end_matches(".gz");
        let mut gzip = archiver_rs::Gzip::open(tarball.as_ref())?;
        gzip.decompress(tar_file.as_ref())?;

        let mut tar = tar::Archive::new(File::open(tar_file)?);
        let mut archive_root_folder = String::new();

        for file in tar.entries()? {
            let mut file = file?;

            let file_size = file.header().size()? as usize;
            if file_size > 0 {
                let archive_path = file.header().path()?.to_str().unwrap().to_string();

                if archive_root_folder.is_empty() {
                    let path = PathBuf::from(archive_path.clone());
                    let path_elems: Vec<&Path> = path.ancestors().collect();
                    archive_root_folder = path_elems[path_elems.len() - 2]
                        .to_str()
                        .unwrap()
                        .to_string();
                }

                let file_output_path =
                    PathBuf::from(output_path.to_string() + "/" + archive_path.as_str());
                std::fs::create_dir_all(file_output_path.parent().unwrap())?;

                let mut buffer = Vec::new();
                buffer.reserve(file_size);
                file.read_to_end(&mut buffer)?;

                let mut out = File::create(file_output_path.as_path())?;
                out.write_all(buffer.as_slice())?;
                out.flush()?;
            }
        }
        std::fs::remove_file(tar_file)?;

        std::fs::write(test_file.as_str(), archive_root_folder.as_str().as_bytes())?;
        Ok(archive_root_folder)
    }

    pub fn build(
        &self,
        package_folder: &str,
        platform: &impl Platform,
    ) -> Result<(), Box<dyn Error>> {
        use std::process::*;

        let mut default_command_line = platform.make_command_line();
        if self.make_cmd_override.is_some() {
            default_command_line = self.make_cmd_override.as_ref().unwrap().to_string();
        }

        let mut command = String::new();
        let mut args = Vec::new();
        let mut is_quoted = false;
        let mut quote_start_index = 0;
        let mut quote_count = 0;

        // FIXME: This can probably be done better, figure out a way to do so
        for (index, mut cmd) in default_command_line
            .split_whitespace()
            .map(|s| s.trim())
            .enumerate()
        {
            if index == 0 {
                command = cmd.to_string();
                continue;
            }

            if cmd.starts_with('\"') {
                if quote_count == 0 {
                    cmd = &cmd[1..];
                    args.push(cmd.trim_end_matches('"').to_string());
                    is_quoted = true;
                    quote_start_index = index;
                }
                quote_count += 1;
            }

            if is_quoted {
                if cmd.ends_with('\"') {
                    quote_count -= 1;
                    if quote_count == 0 {
                        is_quoted = false;
                        cmd = cmd.trim_end_matches('"');
                    }
                }

                if quote_start_index != index {
                    let args_len = args.len();
                    args[args_len - 1].push(' ');
                    args[args_len - 1].push_str(&cmd);
                }
            } else {
                args.push(cmd.to_string());
            }
        }

        println!("Running {} {:?} from {}...", command, args, package_folder);

        let mut command = Command::new(command);
        command
            .env("NO_DEPEND", "Yes")
            .env("EXCLUDE_TARGETS", "iuplua5 iupluaconsole iupluascripter")
            .current_dir(package_folder);

        for (env_key, env_value) in &platform.environment() {
            command.env(env_key, env_value);
        }

        for arg in &args {
            command.arg(arg);
        }

        let mut child = command.spawn()?;
        let status = child.wait()?.code().unwrap();
        if status != 0 {
            return Err(format!("Failed to build {}", package_folder).into());
        }

        Ok(())
    }
}
